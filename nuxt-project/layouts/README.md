## 布局
  - 布局是页面的包装器，包含了多个页面的共同用户界面，如页眉和页脚。布局是使用 `<slot />` 组件来显示页面内容的 Vue 文件。
  - `layouts/default.vue` 文件将被**默认使用**。
  - 自定义布局可以作为页面元数据的一部分进行设置。

### 文档
  - https://nuxt.com.cn/docs/guide/directory-structure/layouts

### 应用场景
  - `Nuxt` 提供了一个布局框架，用于将常见的 `UI` 模式提取为可**重用**的布局。

### 注解
  - 为了获得最佳性能，在使用时，放置在此目录中的组件将通过异步导入自动加载。

### 启用布局
  - 通过在 `app.vue` 中添加 <NuxtLayout>，可以启用布局（这是全局布局）
  ```vue
  <template>
    <NuxtLayout>
      <NuxtPage />
    </NuxtLayout>
  </template>
  ```

### 默认布局
  - 添加 `~/layouts/default.vue`。在布局文件中，页面的内容将显示在 <slot /> 组件中
  ```vue
  <template>
    <NuxtLayout>
      <NuxtPage />
    </NuxtLayout>
  </template>
  ```

### 命名布局
  - 添加 `~/layouts/custom.vue`。
  - 可以直接通过 <NuxtLayout> 的 name 属性覆盖所有页面的默认布局
  ```vue
  <!-- app.vue -->
  <script setup lang="ts">
  // 可以基于 API 调用或登录状态进行选择
  const layout = "custom";
  </script>

  <template>
    <NuxtLayout :name="layout">
      <NuxtPage />
    </NuxtLayout>
  </template>
  ```

### 动态更新布局
  - 可以使用 `setPageLayout` 辅助函数动态更改布局
  ```vue
  <!-- pages/home.vue -->
  <template>
    <section>
      <p>此页面将显示在 /home 路由。</p>

      <button @click="enableCustomLayout">更新布局</button>
    </section>
  </template>

  <script setup lang="ts">
  function enableCustomLayout() {
    setPageLayout('custom')
  }
  definePageMeta({
    layout: false
  })
  </script>
  ```

### 在每个页面上覆盖布局
  - 如果你使用页面，可以通过设置 `layout: false`，然后在页面内部使用 <NuxtLayout> 组件来完全控制布局
  ```vue
  <!-- pages/index.vue -->
  <template>
    <div>
      <NuxtLayout name="custom">
        <h1>欢迎来到首页</h1>
        <AppAlert>
          这是一个自动导入的组件。
        </AppAlert>
      </NuxtLayout>
    </div>
  </template>

  <script setup lang="ts">
  definePageMeta({
    layout: false
  })
  </script>
  ```