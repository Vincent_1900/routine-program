export interface Restaurant {
  /**
   * 定义一个对象类型
   * 该对象可以包含任意属性名（字符串类型）及其对应的属性值
   * - x 表示属性名的占位符，可以用任意字符串替代
   * - string 表示属性名的类型必须是字符串
   * - any 表示属性值的类型可以是任意类型
   */
  [x: string]: any
}