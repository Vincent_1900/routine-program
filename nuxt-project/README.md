### Nuxt 3 Minimal Starter

  Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

### 环境

  node 18+

### 内部集成依赖

  - 状态管理：[Pinia](https://pinia.vuejs.org/)
  - 数据管理 [nuxtjs/strapi](https://strapi.nuxtjs.org/)


