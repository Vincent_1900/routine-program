import { defineStore } from 'pinia'

// 你可以对 `defineStore()` 的返回值进行任意命名，但最好使用 store 的名字，同时以 `use` 开头且以 `Store` 结尾。如：useCounterStore

// 第一个参数是你的应用中 Store 的唯一 ID【注意，不能重复】。如：counter
// defineStore() 的第二个参数可接受两类值：Setup 函数或 Option 对象。此处示例是使用 Option 对象

/**
 * 你可以认为 
 *  - state 是 store 的数据 (data)，
 *  - getters 是 store 的计算属性 (computed)，
 *  - actions 是方法 (methods)
 */
export const useCounterStore = defineStore('counter', {

  /**
   * state 被定义为一个返回初始状态的函数
   */
  state: () => ({
    count: 0,
    userList: {},
  }),

  /**
   * - getters 完全等同于 store 中 state 的计算属性
   * - 推荐使用 箭头函数
   * - 接收 state 作为第一个参数
   */
  getters: {
    // 大多数时候， getter 仅依赖 state
    doubleCount: state => state.count * 2,
    
    // 有时候，getter 也可能会使用其他的 getter
    /**
     * 此时，不推荐使用箭头函数，因为没有 this
     * 这个只能通过 this 去获取其他的 getter
     */
    doublePlusOne() {
      return this.doubleCount + 10
    } 
  },

  /**
   * - action 相当于组件中的 method
   * - action 可用过 this 访问整个 store 实例
   * - action 可以是异步的（可以在其中使用 async/await 调用任何 api，以及其他 action）
   */
  actions: {
    increment() {
      this.count++
    },

    // 使用 异步的示例
    async getUserInfoAsync() {
      try {
        const res = await fetch('https://api.github.com/users/antfu')
        if (res.status === 200) {
          const data = await res.json()
          this.userList = { ...data }
          /**
           * 当你使用 fetch 请求获取数据时，Vue 的响应式系统可能会对这些数据进行代理，以便在数据发生变化时触发组件的重新渲染。
           * 这就是为什么你看到的 userList 是一个 Proxy 对象
           */
        }
      } catch (error) {
        console.log(' -- error -- ', error);
      }
    },
  }
})