import { defineStore } from 'pinia'

// defineStore() 的第二个参数可接受两类值：Setup 函数或 Option 对象。此处示例是使用 Setup 对象

/**
 * ref() 就是 state 属性
 * computed() 就是 getters
 * function() 就是 actions
 */
export const useCounterSetupStore = defineStore('counterSetup', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  const countPlusOne = computed(() => doubleCount.value + 2)
  function increment() {
    count.value++
  }

  // 在 Setup Store 中，需要自己创建自己的 $reset() 方法
  function $reset() {
    count.value = 0
  }

  return {
    count,
    doubleCount,
    countPlusOne,
    increment,
    $reset
  }
})