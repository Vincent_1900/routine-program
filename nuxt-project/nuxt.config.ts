// https://nuxt.com/docs/api/configuration/nuxt-config
// Nuxt附加配置
export default defineNuxtConfig({
  css: ['~/assets/css/reset.css'], // 全局注入并存在于所有页面中 - 本地样式表
  app: {
    // 设置head头部信息，SEO&Meta
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0',
      // 外部样式表
      link: [{ rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css' }]
    },
    // 设置页面过度效果
    pageTransition: { name: 'page', mode: 'out-in' },
    // 设置布局过渡效果
    // layoutTransition: { name: 'layout', mode: 'out-in' }
  },
  vite: {
    // 全局样式导入
    css: {
      preprocessorOptions: {
        // scss
        scss: {
          additionalData: '@use "@/assets/css/_colors.scss" as *;'
        }
      }
    }
  },
  modules:[
    '@pinia/nuxt', // 状态管理 - pinia
    '@nuxtjs/strapi', // 数据管理 - Strapi
    '@nuxtjs/tailwindcss', // 外部样式依赖 - tailwindcss
  ],
  strapi: {
    url: 'http://localhost:1337', // Strapi 服务的 url
    prefix: '/api', // Strapi 服务的前缀（仅在版本为 v4  时有效）
    version: 'v4', // Strapi 服务的版本（v3/v4）
  },
  devtools: { enabled: true }
})
