// 引入express模块
const express = require('express');

const authFunc = require('./wechat/authPlusOne');
// const accessToken = require('./wechat/accessToken');

// 创建app应用对象
const app = express();

// 中间件，接受处理所有消息
app.use(authFunc());

// 监听端口号
app.listen(3000, () => console.log('服务器启动成功'));