/**
 * https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
 * 获取access token
 * 1. access token是微信调用接口全局凭证
 *  1.1 唯一的
 *  1.2 有效期2小时，提前5分钟请求新的access token
 *  1.3 每天最多请求2000次
 * 
 * 2. https请求方式
 *  2.1 请求方法 - GET 
 *  2.2 url - https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
 * {
        grant_type 获取access_token填写client_credential
        appid 第三方用户唯一凭证
        secret 第三方用户唯一凭证密钥，即appsecret
 * }

 * 3. 设计思路
 *  3.1 首次，发送请求获取access token，保存下来（本地文件）
 *  3.2 后续，本地读取文件获取access token，判断是否过期
 *      - 没过期，直接使用
 *      - 过期，重新请求access token，保存并覆盖
 *  
 * 4. 优化思路
 *  4.1 读取本地文件
 *      - 没有文件，发送请求获取access token
 *      - 有文件，读取文件
 *          - 未过期，直接使用
 *          - 过期，重新获取
 */

// 引入config
const config = require('../config');
// 引入request-promise-native
const rp = require('request-promise-native');
// 引入fs模块
const { writeFile, readFile } = require('fs')

const { appID, appsecret } = config;
class AccessToken {

    /**
     * 获取access token
     */
    getAccessToken() {
        const url = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appID}&secret=${appsecret}`;

        // 发送请求
        /**
         * request
         * request-promise-native：返回值时promise对象
         */
        return new Promise((resolve, reject) => {
            rp({ method: 'GET', url, json: true,
            }).then(res => {
                console.log(res);
                /**
                {
                    access_token: '74_Ll2yrHJFZQRHiqsrzL3VMh8EA8IZ_Aa14uiPelN6XyJ9dvEEBwK63uBFdIsecY37-6NG0_CeAiaQfUt3r6_CQI4O30ebYHO_krHixswnAh9qGzleeWmxujKBNg8OBUhAFAZMF',
                    expires_in: 7200
                }
                 */
    
                // 设置access token的过期时间 （提前5分钟）计算方式：(当前时间 + (过期时间-300s)) * 1000ms
                res.expires_in = Date.now() + (res.expires_in - 300) * 1000;
                resolve(res)
            })
            .catch(error => {
                console.log(error);
                reject(error)
            })
        })
    }

    /**
     * 保存access token
     * @param {*} accessToken 
     */
    saveAccessToken(accessToken) {
        return new Promise((resolve, reject) => {
            writeFile('./accessTokenFile', JSON.stringify(accessToken), err => {
                if (!err) {
                    console.log('文件写入成功');
                    resolve();
                } else {
                    console.log('文件写入失败');
                    reject('saveAccessToken失败');
                }
            })
        })
    }

    /**
     * 读取access token
     */
    readAccessToken() {
        return new Promise((resolve, reject) => {
            readFile('./accessTokenFile', (err, data) => {
                if (!err) {
                    console.log('文件读取成功', data);
                    resolve(JSON.parse(data))
                } else {
                    reject('readAccessToken失败')
                }
            })
        })
    }

    /**
     * 判断access token是否有效
     * @param {*} data 
     */
    isVaildAccessToken(data) {
        console.log(' -- data -- ', data);
        // 检测传入的参数是否有效
        if (!data && !data.access_token || !data.access_token.expires_in) {
            // access token无效
            return false;
        }

        // 检测access token是否在有效期内。大于->没过期 | 小于->过期
        return data.expires_in > Date.now();
    }
}


// 模拟流程测试 - 按照第四点-思路优化的方法
const accessTokenObj = new AccessToken()

new Promise((resolve, reject) => {
    // 读取本地文件
    accessTokenObj.readAccessToken()
        .then(res => {
            // 有文件
            // 判断是否有效
            const isVaild = accessTokenObj.isVaildAccessToken(res);
            console.log(' -- isVaild -- ', isVaild);

            // 无效（过期）
            if (!isVaild) {
                accessTokenObj.getAccessToken()
                    .then(res => {
                        console.log(res);
                        // 获取access token成功，则存储
                        accessTokenObj.saveAccessToken(res.access_token);
                        // 返回access token
                        resolve(res);
                    })
                    .catch(err => {
                        console.log(err);
                    })
            } else {
                // 有效，直接返回access token
                resolve(res)
            }
        })
        .catch(err => {
            // 本地获取文件失败（没有文件），重新获取access token
            accessTokenObj.getAccessToken()
                .then(res => {
                    console.log(res);
                    // 获取access token成功，则存储
                    accessTokenObj.saveAccessToken(res.access_token);

                    // 返回access token
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                })
        })
}).then(res => {
    console.log(' -- 最终的对象 res -- ', res);
})
