
// 引入sha!模块
const sha1 = require('sha1');
// 引入config模块
const config = require('../config');

// 验证服务器的有效性
/**
 * 1. 微信服务器知道开发者服务器是哪个
 *      - 测试号管理页面上填写的url - https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index
 *          - 使用 ngrok 内网穿透，将本地端口号开启的服务映射到外网跨域访问的一个网址
 *          - 命令：ngrok http 3000
 *      - token
 *          - 参与微信签名加密的参数
 * 
 * 2. 开发者服务器验证消息是否来自微信服务器
 *      - 服务器计算签名加密，与微信传递过来的signature对比。若相同，则消息来自微信服务器
 *          - 将参加微信加密签名的三个参数（timestamp、nonce、token），组合在一起，按照字典序排序（0-9 & a-z）,组合在一起形成一个数组
 *          - 将数组里所有参数拼接成一个字符串，进行sha1加密（npm i sha1），形成一个signature
 *          - 跟微信传递过来的signature进行对比
 *          - 若一样，说明消息来自微信服务器，并返回 echostr 给微信服务器
 * 
 */

const authFunc = () => {
    return (req, res, next) => {
        console.log(req.url);
        /**
            {
                signature: 'c31c139bf1e9453dcb3aaf80073eeabe754a8e4f', // 微信的加密签名
                echostr: '3787164836433480479', // 微信的随机字符串
                timestamp: '1700219401', // 微信发送请求的时间戳
                nonce: '1927474263' // 微信的随机数字
            }
        */
        // 解构赋值
        const {signature, echostr, timestamp, nonce} = req.query;
        
        // 创建数组存储
        const array = [timestamp, nonce, config.token];
        
        // 1. 将参加微信加密签名的三个参数（timestamp、nonce、token），组合在一起，按照字典序排序（0-9 & a-z），组合在一起形成一个数组
        array.sort(); // [ '1386606526', '1700444612', 'wechatHTML1117' ]
        console.log(array); 

        // 2. 将数组里所有参数拼接成一个字符串
        const str = array.join(''); // 13866065261700444612wechatHTML1117
        // 2.1 sha1加密
        const shaString = sha1(str); // 6f1e5bc84f764a8c205348abca0652a0a54804bc

        // 3. 跟微信传递过来的signature进行对比
        // 3.1 成功，则返回微信的echostr
        shaString === signature && res.send(echostr);

    }
}

module.exports = authFunc;