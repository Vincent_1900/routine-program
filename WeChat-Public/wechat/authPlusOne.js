
// 引入sha!模块
const sha1 = require('sha1');
// 引入config模块
const config = require('../config');

// 验证服务器的有效性
/**
 * 1. 微信服务器知道开发者服务器是哪个
 *      - 测试号管理页面上填写的url - https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index
 *          - 使用 ngrok 内网穿透，将本地端口号开启的服务映射到外网跨域访问的一个网址
 *          - 命令：ngrok http 3000
 *      - token
 *          - 参与微信签名加密的参数
 * 
 * 2. 开发者服务器验证消息是否来自微信服务器
 *      - 服务器计算签名加密，与微信传递过来的signature对比。若相同，则消息来自微信服务器
 *          - 将参加微信加密签名的三个参数（timestamp、nonce、token），组合在一起，按照字典序排序（0-9 & a-z）,组合在一起形成一个数组
 *          - 将数组里所有参数拼接成一个字符串，进行sha1加密（npm i sha1），形成一个signature
 *          - 跟微信传递过来的signature进行对比
 *          - 若一样，说明消息来自微信服务器，并返回 echostr 给微信服务器
 * 
 */

const authFunc = () => {
    return (req, res, next) => {
        console.log('请求方法 - ',req.method);
        console.log(req.query);
        // 解构赋值
        const {signature, echostr, timestamp, nonce} = req.query;
        const shaString = sha1([timestamp, nonce, config.token].sort().join(''));

        /**
         * 微信服务器会发送两种类型的消息给开发者服务器 req.method === 'GET' || 'POST'
         *  1. GET请求
         *      - 验证服务器的有效性
         *  2. POST请求
         *      - 微信服务器会将用户发送的数据以POST请求的方式转发到开发者服务器上
         */

        if (req.method === 'GET') {
            // 验证服务器的有效性
            if (shaString === signature) {
                // 跟微信传递过来的signature进行对比, 成功，则返回微信的echostr
                res.send(echostr);
            } else {
                // 不一样，说明不是微信服务器发送的消息，则返回error
                res.end('error');
            }
        } else if (req.method === 'POST') {
            // 微信服务器会将用户发送的数据以POST请求的方式转发到开发者服务器上
            // 验证消息来自微信服务器
            if (shaString !== signature) {
                // 消息不是微信服务器
                res.end('error');
            }

            console.log(req.query);
        }
    }
}

module.exports = authFunc;