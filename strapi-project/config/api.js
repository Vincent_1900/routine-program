// API 调用的常规设置文件
module.exports = {
  // rest api配置
  rest: {
    defaultLimit: 25, // 默认25条
    maxLimit: 100, // 最大限制100条
    withCount: true, // 显示总数
  },
};
