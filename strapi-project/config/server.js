// 用于定于 Strapi 应用的服务器配置
module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'), // 服务器运行的端口
  port: env.int('PORT', 1337),
  app: {
    keys: env.array('APP_KEYS'),
  },
  webhooks: {
    populateRelations: env.bool('WEBHOOKS_POPULATE_RELATIONS', false),
  },
});
