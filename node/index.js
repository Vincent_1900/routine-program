const cheerio = require('cheerio'); // 解析 HTML 文档的工具
const axios = require('axios');
const http = require('https');

const movies = [];

// axios 的请求方式
async function axiosRequest(start) {
  const response = await axios(`https://movie.douban.com/top250?start=${start}`)
  const $ = cheerio.load(response.data)

  $('.grid_view .item').each(function () {
    const title = $(this).find('.title').text().trim();
    const rating = $(this).find('.rating_num').text().trim();
    const poster = $(this).find('.pic img').attr('src');
    const desc = $(this).find('.quote .inq').text().trim();

    movies.push({ title, rating, desc, poster });
  });

  console.log(' -- movies -- ', movies);
  console.log(' -- length -- ', movies.length);
}

// https 的请求方式
function httpRequest(start) {
  http.get(`https://movie.douban.com/top250?start=${start}`, res => {
    let data = '';
    
    // 收到响应数据时触发
    res.on('data', chunk => {
      data += chunk;
    });

    // 收到完整响应数据时触发
    res.on('end', () => {
      const $ = cheerio.load(data)
      $('.grid_view .item').each(function () {
        const title = $(this).find('.title').text().trim();
        const rating = $(this).find('.rating_num').text().trim();
        const poster = $(this).find('.pic img').attr('src');
        const desc = $(this).find('.quote .inq').text().trim();
    
        movies.push({ title, rating, desc, poster });
      });
    
      console.log(' -- http - movies -- ', movies);
      console.log(' -- length -- ', movies.length);
    })
  })
}

for (let index = 0; index < 250; index++) {
  if (index % 25 == 0) {
    httpRequest(index)
  }
}


