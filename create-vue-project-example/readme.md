**vue2-project 是一个使用@vue/cli脚手架搭建的vue2项目**
**vue3-project 是一个使用@vue/cli脚手架搭建的vue3项目**


**`Vue` 脚手架相关**

```js
/* -- 脚手架安装 -- */
- `npm i vue-cli -g`
- `npm i @vue-cli -g`

/* -- 创建项目 -- */
- `vue init webpack 项目名`
- `vue create 项目名`

/* -- 说明 -- */
- vue2 脚手架只能创建 vue2 项目
- vue3 脚手架 vue2/3 项目都可创建，在使用 `vue create ***` 之后可以进行选择
```



**初始化依赖安装**

```js
1. 全局安装 `vue`
    1.1 说明
	    - vue cli 依赖于全局安装的 vue 来提供其核心功能
    	- 具体的，vue cli 使用了一个名为 `@vue/cli`
    1.2 安装
    	- `npm i vue -g` // 全局安装
    1.3 安装后查看版本号
    	- `npm list vue -g`
    
2. 全局安装 `@vue/cli`
    2.1 说明
    	- 项目脚手架搭建：`Vue CLI` 可通过简单的命令帮助创建一个基于 `Vue.js` 的项目结构，包括项目的目录结构、配置文件等。
        - 开发服务器：`Vue CLI` 内置了开发服务器，支持 热重载 和 实时开发预览，方便开发阶段的调试。
        - 插件体系：`ESLint`、`TS` 的等集成
        - 基于 `webpack` 构建和打包流程 
    2.2 安装
    	- `npm i -g @vue/cli`
    	- 【注】：如果安装速度过慢，可以将 `npm` 仓库地址改为淘宝镜像 - `npm config set registry https://registry.npm.taobao.org --global`。安装完毕后查看是否安装完成 - `npm config get registry`
    2.3 安装后查看版本号
    	- `vue -V / vue --version`
```




### 路由


**路由引入**

- `vue2` 路由引入

```js
1. 安装指定版本的 `vue-router`
    1.1 推荐使用 `vue-router 3.x` 版本。若大于4.x，则部分功能无法在 `vue2` 中正常使用
    1.2 `npm i vue-router@3.5.1 -s`
2. 创建统一管理路由的入口文件
    2.1 `vue2-project/src/router/index.js`
3. `vue` 项目入口文件 `main.js` 引入路由入口文件
4. `App.vue` 中配置 自定义路由组件 - `router-view`
    4.1 `router-view` 将显示与 `URL` 对应的组件。可以把它放在任何地方，以适应项目的布局。（一般是在项目的根组件 - `App.vue`）
```


- `vue3` 路由引入

```js
1. 安装：`npm i vue-router@4`
2. 创建统一管理的路由入口文件 `src/router/index.js`（注意这里的配置，目前实践外部引入模块不成功）
3. `vue` 项目入口文件 `main.js` 引入路由入口文件
4. `App.vue` 中配置 自定义路由组件 - `router-view`
5. `router-link` 组件的 `to` 属性接收路由的路径，`router-view` 组件的 `name` 属性接收路由的名称。
```



**路由的跳转方式**


- `vue2` 路由的跳转方式

```js
1. 使用 自定义组件 - `router-link`
    - `<router-link to="/home"></router-link>`。内部会调用 `router.push()` 方法进行跳转
2. 使用 `router.push()` 方法
    - `router.push({ path: '/home' })`
    - `router.push({ name: 'home' })`
    - `router.push({ path: '/home', query: { id: 1 }})`，(path + query) ，参数接收 - `this.$route.query`
    - `router.push({ name: 'home', params: { id: 1 }})`，(name + params)，参数接收 - `this.$route.params`
    - `router.push()`
3. 使用 `router.replace()` 方法
    - `router.replace({ path: '/home' })`
    - `router.replace({ name: 'home' })`
    - `router.replace({ path: '/home', query: { id: 1 }})`
    - `router.replace({ name: 'home', params: { id: 1 }})`
    - `router.replace()`
```


- `vue3` 路由的跳转方式

```js
【注】`vue-router@4.x` 不再支持 `params` 传参方式
```



**路由传参**

- 官方文档：https://router.vuejs.org/zh/introduction.html


- `vue-router` 可以通过 `props` 进行传参

```js
1. 路由配置 - 动态参数（推荐的方式）
    - `path: '/', name: 'home' component: Home, props: (route) => { return { name: route.query.name }}`
    - `path: '/', name: 'home' component: Home, props: (route) => { return { name: route.params.name }}` -【注】`vue3` 已不推荐这种写法
2. 使用 `router.push()` 方法
    - `router.push({ path: '/', query: { name: '' }})`
    - `router.push({ name: 'home', params: { name: '' }})` -【注】`vue3` 已不推荐这种写法
3. 目标组件接收参数
    - `vue2` - `props: { name: String } console.log(this.name)`
    - `vue3` - `const props = defineProps({ name: String }) console.log(props.name)`
```


- 直接通过 `$route` 对象获取参数

```js
1. 路由正常配置
2. 使用 `router.push()` 方法
    - `router.push({ path: '/', query: { name: '' }})`
    - `router.push({ name: 'home', params: { name: '' }})` -【注】`vue3` 已不推荐这种写法
3. 目标组件接收参数
    - `$route.query.name`
    - `$route.params.name` -【注】`vue3` 已不推荐这种写法
```


- `$router` 与 `$route`
    - `$router` - 全局路由对象，提供对路由的访问
    - `$route` - 当前路由对象（活跃），提供对当前路由的访问
    - 使用方式
    ```js
    // vue2
    this.$router.push/replace/go
    this.$route.query/params

    // vue3
    import { useRouter, useRoute } from 'vue-router'
    const router = useRouter()
    const route = useRoute()

    router.push/replace/go
    route.query
    ```




### 接口请求 - axios

- 官方文档：
    - 英文：https://axios-http.com/docs/intro
    - 中文：https://www.axios-http.cn/docs/intro

1. 定义：`Axios` 是一个基于 `promise` 的 `HTTP` 库，可以用在浏览器和 `node.js` 中。在服务端使用原生 `node.js http` 模块，在客户端（浏览器）则使用 `XMLHttpRequest`
2. 安装：`npm i axios`
3. 初始化
    3.1 `vue3-project/src/api/requestBase.js` - 创建 `axios` 实例，按需使用拦截器
    3.2 `vue3-project/src/api/sendApiRequest.js` - 统一管理接口请求




### ESlint相关约束检查关闭

- 中文文档：https://nodejs.cn/eslint/getting-started/
- vue-eslint: https://eslint.vuejs.org/rules/require-default-prop.html

1. 新建 `.eslintrc.js` 文件，在文件写入对应的配置
2. 问题

    - https://stackoverflow.com/questions/54337576/eslint-import-meta-causes-fatal-parsing-error

    - 报错：`error  Parsing error: Unexpected token <`
    ```js
    【分析】这是因为 `ESlint` 无法正确解析 `Vue` 单文件组件导致的。
    【解决步骤】
        1. 检测是否安装 `vue-eslint-parser`（这是 `Vue` 文件 `ESlint` 解析器，将 `Vue` 文件中的模版、脚本、样式解析成抽象语法树 `AST`，以便 `ESlint` 可以对 `AST` 进行静态代码分析）
        2. 安装 `vue-eslint-parser` - `npm i vue-eslint-parser`
        3. 在 `.eslintrc.js` 添加 `parser` 字段，值为 `vue-eslint-parser`
    【拓展】
        1. `eslint-plugin-vue` 与 `vue-eslint-parser` 的作用分别是什么
            - `eslint-plugin-vue` - 这是一个 `ESLint` 插件，专门为 `Vue` 提供了一系列的规则（提供了 `Vue.js` 项目中的代码规范和最佳实践）
            - `vue-eslint-parser` - `ESlint` 解析器，用于解析 `Vue` 语法（使 `ESLint` 能够理解和分析 `Vue` 项目中的代码）
    ```

    - `ESLint` 配置中，`parser` 和 `parserOptions.parser` 的关系
    ```js
    作用都是用于指定代码解析器，但是 `parserOptions.parser` 的作用范围更广，优先级更高
    ```