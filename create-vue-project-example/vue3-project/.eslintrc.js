// ESlint的配置文件
module.exports = {
    root: true,
    // 代码指定环境，可包含多个环境
    env: {
        browser: true,
        node: true,
        es6: true,
        commonjs: true
    },
    // 继承其他配置文件的规则，可包含多个配置
    extends: [],
    // 配置具体的规则和行为
    rules: {
        'no-unused-vars': 'off'
    },
    parser: 'vue-eslint-parser', // 支持Vue单文件组件的解析器。要安装对应的依赖 `npm i vue-eslint-parser --save-dev`
    // 指定要使用的ESlint插件，可包含多个插件
    plugins: ['vue'],
    // 配置解析器的选项
    parserOptions: {
        // parser: 'babel-eslint', // 指定解析器。对于TS项目，可以设置为@typescript-eslint/parser
        ecmaVersion: 2020, // 指定ES版本，解决 `Parsing error: Unexpected token import`
        sourceType:'module', // 指定模块类型。module表示代码是ES模块，script表示传统脚本
    },
    // 定义全局变量（告诉ESlint这些全局变量是已经定义的，不要报错）
    globals: {
        // $: 'readonly',
        // jQuery: 'readonly',
    }
}