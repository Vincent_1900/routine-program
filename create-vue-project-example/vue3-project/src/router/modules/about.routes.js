export default [
    {
        path: '/about',
        name: 'about',
        components: () => import(/* webpackChunkName: "AboutPage" */ '../../components/AboutPage.vue'),
    }
]