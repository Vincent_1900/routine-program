export default [
    {
        path: '/',
        name: 'home',
        components: () => import(/* webpackChunkName: "HomePage" */ '../../components/HomePage'),
    }
]