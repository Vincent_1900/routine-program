import { createRouter, createWebHistory } from 'vue-router'

// import home from './modules/home.routes'
// import about from './modules/about.routes'

// const allRouter = [...home, ...about]

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../components/HomePage.vue'),
        props: (route) => {
            return {
                name: route.query.name
            }
        }
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('../components/AboutPage.vue'),
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router

