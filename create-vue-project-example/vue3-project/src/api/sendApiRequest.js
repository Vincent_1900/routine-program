import request from './requestBase'

export function chatGPTRequest (params) {
    console.log('params -- ', params);
    return request.post('/v1/engines/davinci-codex/completions', params)
}