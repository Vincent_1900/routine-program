import axios from 'axios'

const baseUrl = '' // 请求的url - chatGPT

// 创建一个 axios 的实例
const instance = axios.create({
    baseURL: baseUrl,
    timeout: 5000,
    headers: { 
        'Content-Type': 'application/json',
    }
})

// 请求拦截
instance.interceptors.request.use(
    config => {
        // 发送请求之前做一些处理
        console.log(' -- config -- ', config);
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

// 响应拦截
instance.interceptors.response.use(
    response => {
        // dataAxios 是 axios 返回数据中的 data
        const dataAxios = response.data
        return dataAxios
    },
    error => {
        console.log('err' + error) 
        return Promise.reject(error)
    }
)

export default instance