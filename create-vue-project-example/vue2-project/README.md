# vue2-project

在 vue2 中使用 composition-api

1. 安装
    - `npm i @vue/composition-api`
2. 注册
    ```js
    import  Vue from 'vue'
    import VueCompositionApi from '@vue/compistion-api'
    Vue.use(VueCompositionApi)
3. 组件中使用
    ```js
    import { ref, reactive, computed, watch, onMounted } from '@vue/composition-api'  
    ```



# 遇见的问题及解决方案
1. https://blog.csdn.net/qq_53499304/article/details/123578998