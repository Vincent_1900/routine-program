import Vue from 'vue'
import App from './App.vue'
import VueCompisitionApi from '@vue/composition-api'

// 路由模块的引入
import router from './router'

Vue.config.productionTip = false
Vue.use(VueCompisitionApi)

new Vue({
  render: h => h(App),
  router, // 将 vue router 实例挂载到 app 实例上
}).$mount('#app')
