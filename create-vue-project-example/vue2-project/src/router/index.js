import Vue from 'vue'
import VueRouter from 'vue-router'

import about from './modules/about.routes'
import hello from './modules/hello.routes'

// 注册 vue-router 中的所有组件
Vue.use(VueRouter)

const allRouter = [...hello, ...about]

// 创建 vue router 实例并定义路由
const router = new VueRouter({
    mode: 'history',
    routes: allRouter
})

export default router