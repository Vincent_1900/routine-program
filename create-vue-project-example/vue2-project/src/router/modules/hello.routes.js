export default [
    {
        path: '/hello',
        name: 'helloPage',
        component: () => import(/* webpackChunkName: "home" */ '../../components/HelloWorld')
    }
]