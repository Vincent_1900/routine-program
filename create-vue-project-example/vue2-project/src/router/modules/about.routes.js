export default [
    {
        path: '/',
        name: 'aboutPage',
        component: () => import(/* webpackChunkName: "home" */ '../../components/AboutPage'),
        props: (route) => {
            return {
                name: route.query.name,
                age: route.params.age
            }
        }
    }
]