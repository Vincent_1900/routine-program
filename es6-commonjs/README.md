## `ES6` 与 `CommonJS` 的对比

`ES6`（`ECMScript 2015`）和 `CommonJS` 都是 `JavaScript` 中模块化的解决方案，但他们也有一些关键的区别。



### `ES6`模块



#### 语法

`ES6` 模块使用 `import` 和 `export` 语法进行导入导出

```js
// 导出 myModule
export const myFunction = () => {};
// 导入
import { myFunction } from './myModule'
myFunction()

// 或者

// 导出
const myFunction = () => {};
export default = { myFunction }
// 导入
import moduleA from './myModule'
moduleA.myFunction()
```



#### 静态

`ES6`的语法在代码**编译阶段**就已经确定，并且在运行时不会发生变化

**静态语法分析**

- `ES6`的新语法特性在**代码编译阶段**就被静态的分析和确定。这与`JavaScript`在执行过程中动态解析代码并执行不同（运行时解析代码）。**静态分析**意味着在**编译阶段**就能够确定代码结构和语法是否正确，而不需要等到运行时才发现错误。

**不可变性**

- `ES6`引入一些新特性，如`const`、`let`关键字，箭头函数等，强调了变量的不可变性。这种不可变性有助于代码在静态分析阶段更容易理解和优化。

**模块化**

- `ES6`引入了模块化系统，其中`export`  和`import`语句使得模块之间的依赖关系在静态分析阶段就能够被确定。这使得编译器和工具能够更好的进行代码优化和减少包的大小。

总体而言，`ES6`的静态性质使得代码更容易被理解、分析和优化。这种静态性质并**不是指所有的`ES6`特性**在编译阶段就决定的，而是强调**一些新特性**在代码解析时就能确定，不会在运行时动态变化，从而提高代码的可读性和可维护性。



#### 单例

`ES6`通过**模块的导入和导出**，实现单例。

当在一个模块中定义变量或函数时，该模块的导出会成为单例的实例

```js
// singleModule.js
const instance = {}
export function getInstance() {
    return instance
}
```

在其他文件中导入该模块，将得到相同的实例

```js
import { getInstance } from './singleModule'
const myInstance = getInstance()
```

这确保了`singleModule`模块在整个应用程序中只有**一个实例**



#### 支持异步加载

`ES6`的异步加载主要通过两种机制实现：`Promise` 和 模块系统

- **`Promise`**

  `ES6` 引入了 `promise` 对象，它代表一个**异步操作**的最终完成或失败的值

  ```js
  function fetchData() {
      return new Promise((resolve, reject) => {
          // 异步操作，例如ajax
          setTimeout(() => {
              resolve()
          })
      })
  }
  
  fetchData().then().catch(error => {})
  ```

- **模块系统**

  `ES6`引入了一种原生的模块系统，可以通过 `import` 和 `export` 进行模块导入和导出。这种模块系统支持异步加载，可以在运行时**按需加载模块**（少见）

  ```js
  // 模块文件 moduleA.js
  export const data = 'some data';
  
  // main.js
  import('./moduleA.js')
      .then(res => {
      	console.log(res.data)
  	})
      .catch(error => {
      	console.log(error)
  	})
  
  // import('./moduleA.js') 返回一个 Promise。这使得模块可以在需要的时候按需加载，而不是一开始就加载整个应用程序。
  ```




### `CommonJS`



#### 语法

`CommonJS` 使用 `require` 和 `module.exports` 进行导入和导出

```js
// 导出
function myFunction() {}
module.exports = { myFunction }
// 或者
export.myFunction = function() {}

// 导入
const { myFunction }  = require('./myModule')
```



#### 动态

`CommonJS` 模块的加载和执行是在**运行时动态发生**的，而不是在代码编译时静态确定的

**运行时加载**

- 在 `CommonJS` 模块系统中，模块的加载是在运行时动态进行的。当执行到 `require` 语句时，才会加载所需的模块，并且模块的内容在第一次加载时执行。

  ```js
  // CommonJS 模块
  const otherModule = require('./otherModule')
  ```

**模块缓存**

- `CommonJS`模块在第一次加载时，会执行模块中的代码，并**将导出的内容缓存起来**。如果同一个模块被多次 `require`， 除了第一次加载时执行并缓存结果，后续的 `require` 只会使用缓存，**不会执行**。

  ```js
  // moduleA.js
  console.log('Module A is being executed.')
  const data = 'data from Module A'
  module.export = data
  
  // moduleB.js
  console.log('Module B is beding executed.')
  const dataFromModuleA = require('./ModuleA')
  console.log('Data in Module B:', dataFromModuleA)
  
  // main.js
  require('./moduleB') 
  require('./moduleB') // 不再输出，直接使用缓存的结果。说明没有再次执行 moduleA 中的逻辑
  ```

- 执行逻辑分析

  ```bash
  `main.js` 第一次 `require('./moduleB')` 时，打印
  
  Module B is being executed.
  Module A is being executed.
  Data in Module B: Data from Module A
  
  并缓存 moduleA/B 的执行结果
  
  在 `main.js` 中第二次 `require('./moduleB')` 时，不会再次执行 moduleB 的代码，而是直接从缓存中获取导出的结果。所以不会有打印语句输出
  ```

**动态路径**

- `CommonJS` 允许在 `require` 中使用动态的路径表达式。这意味着你可以在运行时根据条件或变量的值动态决定加载哪个模块。

  ```js
  const moduleName = someCondition ? 'moduleA' : 'moduleB';
  const dynamicModule = require(moduleName);
  ```

总而言之，`CommonJS`模块系统的动态性使得模块的加载和执行更灵活。在服务端得到广泛应用。在客户端，由于这种动态加载性质，不太适合（更适合`ES6`模块）

 

#### 同步加载

**阻塞式加载**

- 执行 `require` 语句时，会阻塞其后续代码的执行（模块的同步性）

**模块执行顺序**

- 如果 `模块A` 包含了对 `模块B` 的 `require`，那么 `模块B` 会在 `模块A` 中的 `require` 语句执行时立即加载和执行，而不会等到 `模块A` 执行完才会去加载 `模块B`