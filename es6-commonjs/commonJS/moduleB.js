// moduleB.js
console.log('Module B is being executed.'); // 这句只会在第一次 require 时执行
const dataFromModuleA = require('./moduleA');
console.log('Data in Module B:', dataFromModuleA);
