// ---【Promise】--- //
console.log('---【Promise】---');
const p = new Promise((resolve, reject) => {
    setTimeout(() => {
        const randomNum = Math.random() * 10
        console.log('setTimeout', randomNum)

        if (randomNum > 5) {
            resolve('success')
        } else {
            reject('error')
        }
    }, 1000)
})

p.then((value) => {
    console.log('then - ', value)
}).catch(error => {
    console.log('catch - ', error)
})



// ---【Generator】--- //
console.log('---【Generator】---');
function* func(){
    console.log("one");
    yield '1';
    console.log("two");
    yield '2'; 
    console.log("three");
    return '3';
}

// 创建一个 Generator 对象
const f = func()

const step1 = f.next();
console.log(step1);
// one
// {value: "1", done: false}

const step2 = f.next();
console.log(step2);
// two
// {value: "2", done: false}

const step3 = f.next();
console.log(step3);
// three
// {value: "3", done: true}

const step4 = f.next();
console.log(step4);
// {value: undefined, done: true}


// Generator应用场景 - 实现异步操作流程控制
function* sequentialAsyncTask() {
    const result1 = yield asyncOperation1()
    const result2 = yield asyncOperation2(result1)
}

const generator = sequentialAsyncTask();
generator.next().value.then(result1 => {
    generator.next(result1).value.then(result2 => {
        console.log(result2)
    })
})


// ---【async/await】--- //
console.log('---【async/await】---');
async function sequentialAsyncTask() {
    const result1 = await asyncOperation1()
    const result2 = await asyncOperation2(result1)
}

// 在 async 函数中，可以直接使用 await 等待异步操作的结果，而不需要手动调用 next()
// 从这个层面上来说， async/await 是 对 Generator 函数的一种改进和简化