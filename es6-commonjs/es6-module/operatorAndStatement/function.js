// --- 函数参数的扩展 --- // 

//【默认参数】
function fn (name, age = 18) {
    console.log(name + ',' + age);
}
fn('Tom', 20); // Tom,20
fn('Tom'); // Tom,18
fn('Tom', ''); // Tom

//【注意点】使用函数默认参数时，不允许有同名参数
// 不报错
function fn1 (name, name) {}

// 报错 SyntaxError: Duplicate parameter 'name' not allowed in this context
function fn1 (name, name, age = 18) {}

//【注意点】只有在未传递参数，或者参数为 undefined 时，才会使用默认参数。null 值被认为是有效的值传递
fn('Tom', null); // Tom,null

// 函数参数默认值存在暂时性死区，在函数参数默认值表达式中，还未初始化赋值的参数值无法作为其他参数的默认值
function fn2(x, y = x) {
    console.log(x,y);
}
fn2(1); // 1 1

function fn3(x=y) {
    console.log(x);
}
fn(); // ReferenceError: y is not defined


//【不定参数】
function f(...values) {
    console.log(values.length);
}
f(1, 2) // 2
f(1, 2, 3) // 3




// --- 箭头函数 --- //
const arrowFn = x => x * 2;
console.log(arrowFn(1)); // 2

// 返回一个对象
const arrowFn1 = () => ({x: 1});
console.log(arrowFn1()); // { x: 1 }
