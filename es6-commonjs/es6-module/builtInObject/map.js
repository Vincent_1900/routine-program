//  ----- Map 的声明和赋值 ----- //

const myMap = new Map();

const keyString = 'keyString'; // key 是一个字符串
const keyNumber = 1; // key 是一个数字
const keyObj = {}; // key 是一个对象
const keyFunction = function() {} // key 是一个函数

myMap.set(keyString, 'keyString')
myMap.set(keyNumber, 'keyNumber')
myMap.set(keyObj, 'keyObj')
myMap.set(keyFunction, 'keyFunction')
myMap.set(NaN, 'NaN') // key 是一个NaN值



//  ----- Map 的遍历 ----- //

// for ... of
for (let [key, value] of myMap) {
  console.log(key, value);
}

// forEach
myMap.forEach((value, key) => {
  console.log(key, value);
});

// 输出
/**
 * keyString keyString
 * 1 keyNumber
 * {} keyObj
 * [Function: keyFunction] keyFunction
 * NaN NaN
 * */
