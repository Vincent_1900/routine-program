// --- Set 的声明和赋值 --- //
const mySet = new Set();

mySet.add(1)
mySet.add(1)
mySet.add('string')
mySet.add({name: 'Miss li', gender: 'male'})
mySet.add({name: 'Miss li', gender: 'male'})
mySet.add([1, 2, 3])
mySet.add([1, 2, 3]); // 对象、数组之间引用不同不恒等，即使值相同，Set也能存储
mySet.add(NaN)
mySet.add(NaN); // 只存在一个NaN（NaN 与 NaN 是不恒等的，但是在 Set 中只能存一个，不重复）
mySet.add(undefined);
mySet.add(undefined); // 只存在一个undefined（undefined 与 undefined 是恒等的，所以不重复）

console.log(mySet);

/**
Set(8) {
    1,
    'string',
    { name: 'Miss li', gender: 'male' },
    { name: 'Miss li', gender: 'male' },
    [ 1, 2, 3 ],
    [ 1, 2, 3 ],
    NaN,
    undefined
}
*/


// --- Set 对象的作用 --- //

// 数组去重
const arraySet = new Set([1, 1, 2, 3, 4, 4, 5]);
console.log([...arraySet]); // [ 1, 2, 3, 4, 5 ]

// 并集
const a = new Set([1, 2, 3]);
const b = new Set([2, 3, 4]);
console.log([...new Set([...a, ...b])]); // [ 1, 2, 3, 4 ]

// 交集
const intersection = new Set([...a].filter(x => b.has(x)));
console.log([...intersection]); // [ 2, 3 ]

// 差集
const difference = new Set([...a].filter(x => !b.has(x)))
console.log([...difference]); // [ 1 ]