// --- Proxy 基本用法 --- //

function createProxy(target, handler) {
  return new Proxy(target, handler)
}

// 目标对象
const target = {
  name: 'Tom',
  age: 20,
  info: {
    city: 'Shanghai',
    country: 'China'
  }
}

const handler = {
  get (target, key) {
    console.log('get --- ', key);
    return target[key];

    // 结合 Reflect 使用 return Reflect.get(target, key);
  },
  set (target, key, value) {
    console.log('set --- ', key, value);
    target[key] = value;

    // 结合 Reflect 使用， return Reflect.set(target, key, value);
  }
}

// 代理对象
const proxyObj = createProxy(target, handler);
console.log(proxyObj.name); // 调用 Proxy 的 get 方法

proxyObj.tel = '123456789'; // 调用 Proxy 的 set 方法
console.log(proxyObj.tel);

console.log(' -- target -- ', target);
console.log(' -- proxyObj -- ', proxyObj); // 代理对象是对目标对象的一个浅拷贝，指向同一个内存地址



// ---- 应用场景 ---- //

// 对象赋值的检查
const handler2 = {
  set (target, key, value) {
    if (key === 'age') {
      if (typeof value !== 'number') {
        throw new TypeError('the age is not an number');
      }
      if (value > 200) {
        throw new RangeError('the age seems invalid');
      }
    }
    target[key] = value
  }
}

const proxyObj2 = createProxy(target, handler2);
proxyObj2.age = 21

console.log('proxyObj2 --- ', proxyObj2);


// 对 localStorage 的封装
const localStorageProxy = new Proxy(localStorage, {
  get (target, key) {
    // 读取 localStorage 的逻辑
    console.log(`Reading key ${key}`);
    const value = target.getItem(key);
    try {
      return JSON.parse(value);
    } catch(error) {
      throw new Error('the value is not a valid JSON');
    }
  },
  set (target, key, value) {
    // 存储 localStorage 的逻辑
    console.log(`Writing key ${key} with value ${value}`);
    try {
      target.setItem(key, JSON.stringify(value))
    } catch (error) {
      throw new Error(error);
    }
    return value
  },
  deleteProperty (target, key) {
    // 删除 localStorage 的逻辑
    console.log(`Deleting key ${key}`);
    target.removeItem(key);
  }
})

localStorageProxy.myKey = 'Hello, Proxy!'; // 写入
console.log(localStorageProxy.myKey); // 读取
localStorageProxy.deleteProperty(myKey); // 删除

