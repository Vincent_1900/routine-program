// --- 【字符串】 --- //

// 子串识别
let string = "apple,banana,orange";
console.log(string.includes("banana")); // true
console.log(string.startsWith("apple")); // true
console.log(string.endsWith("apple")); // false
console.log(string.startsWith("banana",6)); // true 指定起始位置

// 字符串重复
console.log(string.repeat(2)); // apple,banana,orangeapple,banana,orange
console.log(string.repeat(2.8)); // 向下取整 2，apple,banana,orangeapple,banana,orange
console.log(string.repeat(-0.5)); // 识别为0
console.log(string.repeat(NaN)); // 识别为0
console.log(string.repeat('1')); // 参数为字符串，自动转换为数字
// console.log(string.repeat(-1)); // 负数，报错
// console.log(string.repeat(Infinity)); // Infinity，报错

// 字符串补全
console.log("orange".padStart(7, 'W')); // 补全的参数个数要大于原来字符串的长度
console.log("orange".padEnd(7, 'W')); // 补全的参数个数要大于原来字符串的长度

// 模版字符串
console.log(`${string}, so much fruit`); // apple,banana,orange, so much fruit


// --- 【对象】 --- //

// 属性简写
const age = 12;
const name = "Tom";
const person = { age, name };
console.log(person);

// 方法名简写
const person2 = {
  sayHi () {
    console.log("Hi");
  },
}
person2.sayHi();

// 属性名表达式
const person3 = {
  ["he" + "llo"]: "world"
}
console.log(person3.hello); // world


// 对象拓展运算符
const person4 = {
  name: "Tom",
  age: 12,
}
const someone1 = { name: "Mike", gender: "male", ...person4 } // { name: 'Tom', gender: 'male', age: 12 }
const someone2 = { ...person4, name: "Mike", gender: "male", } // { name: 'Mike', age: 12, gender: 'male' }
console.log(someone1);
console.log(someone2);


// assgin - 对象拷贝
const target = {a: 1}
const sourceObj1 = {a: 1}
const sourceObj2 = {b: 1, c: { d: 2 }}
Object.assign(target, sourceObj1, sourceObj2)
console.log(target); // { a: 1, b: 1, c: { d: 2 } }
target.c.d = 200
target.b = 100
console.log(sourceObj2); // { b: 1, c: { d: 200 } }

// is
Object.is("q", "q");      // true
Object.is(1, 1);          // true
Object.is([1], [1]);      // false
Object.is({q: 1}, {q: 1});  // false



// --- 【数组】 --- //
console.log(Array.of(1,2,3,4)); // [ 1, 2, 3, 4 ]
console.log(Array.from([1,2, undefined])); // [ 1, 2, undefined ]

let arr = Array.of(1,2,3,4)
console.log(arr.find(item => item > 2)); // 3
console.log(arr.findIndex(item => item > 2)); // 2
console.log(arr.fill('a', 0, 3)); // [ 'a', 'a', 'a', 4 ]
for (let [key, value] of arr.entries()) {
  console.log(`${key}-${value}`); // 0-a 1-a 2-a 3-4
}
for (let key of arr.keys()) {
  console.log(key); // 0,1,2,3
}
for (let value of arr.values()) {
  console.log(value); // a,a,a,4
}
console.log(arr.includes(1)); // false
console.log([1,[2,[3]]].flat()); // [ 1, 2, 3 ]
console.log([1,[2,[3]]].flat(1)); // [ 1, 2, [ 3 ] ]
