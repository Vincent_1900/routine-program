const obj1 = {
  name: "Tom",
  age: 24,
  // 使用 getter 方法定义对象属性。`info` 被定义为一个 getter 方法，可以通过 obj1.info 的方式来访问该属性
  // info 是一个属性
  get info () { 
    return `${this.name} - ${this.age}`
  },
  set info (value) {
    return this.age = value
  }
}


// --- get方法 --- //
Reflect.get(obj1, 'name'); // "Tom"

const obj2 = {
  name: "Jerry",
  age: 20,
}
console.log(Reflect.get(obj1, 'info', obj2)); // Jerry - 20

// 异常情况
// Reflect.get(obj1, 'birth'); // 访问不存在的属性，返回 undefined
// Reflect.get(1, 'name'); // 访问的不是对象时，报错 TypeError
// Reflect.get(objTest, 'name'); // 访问不存在的对象， 报错 ReferenceError


// --- set方法 --- //
Reflect.set(obj1, 'age', 25); // true
console.log(obj1.age); // 25

Reflect.set(obj1, 'info', 1 , obj2); // 访问 obj1.set 方法，this 会绑定 obj2，所以会修改 obj2 的属性
console.log(obj1.age); // 25
console.log(obj2.age); // 1

Reflect.set(obj1, 'age', ); // true
console.log(obj1.age); // undefined


// --- has方法 --- //
console.log(Reflect.has(obj1, 'name')); // true


// --- deleteProperty方法 --- // 
console.log(Reflect.deleteProperty(obj1, 'name')); // true
console.log(Reflect.deleteProperty(obj1, 'gender')); // 属性不存在时，也会返回 true
