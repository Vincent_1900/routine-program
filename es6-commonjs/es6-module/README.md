## ES6模块

`ES6` 全称为 `ECMAScript 6.0`，是 `JavaScript` 的一个新标准，2015.06发版

**运行环境**

各大浏览器目前都支持 `ES6` 的新特性，其中 `Chrome` 和 `Firefox` 浏览器对 `ES6` 新特性最友好，`IE7~11` 基本不支持 `ES6`。



### 声明与表达式


#### let 与 const

- 共性
    - `let` 和 `const` 声明的变量都具有**块级作用域**（仅在声明的块内有效），都存在**暂时性死区**（声明前访问会导致 `ReferenceError`，不存在变量提升）

- 区别
    - `let` 可以重复声明，`const` 不能重复声明
    - `let` 可以不赋值，`const` 必须赋值


#### 结构赋值

- 数组结构的解构
- 对象结构的解构


#### Symbol

- `Symbol` 表示独一无二的值，一般用于标识一个值，不会重复，不会被 `JSON.stringify` 转换为字符串，不会被 `JSON.parse` 解析为对象，不会被 `eval` 语句解析为表达式，不会被 `with` 语句引用，不会被 `delete` 语句删除，不会被 `typeof` 语句检测类型 ......



### 内置对象


#### Map 与 Set

两者都用于存储集合

**Map**
    - 键值对的集合：`Map` 是一组键值对的**有序列表**（按照插入的顺序返回），其中每个键唯一，而值不唯一
    - 键可以是任意类型：在 `Map` 中，键可以是任何类型，包括 基本数据类型（`string`、`number`、`boolean`、`symbol`、`bigint`、`null`、`undefined`、）、引用类型（对象和数组）、`NaN`
    - 尺寸属性： `size` 属性可以获取 `Map` 中键值对的数量
    - 方法：`keys()`、`values()`、`entries()`、`forEach()`、`clear()`、`has()`、`get()`、`set()`、`delete()`
    - 遍历： `for of`、 `forEach`

**Set**
    - 值的集合：`Set` 是一个无序的集合，其中每个值唯一，不能重复
    - 值可以是任意类型：在 `Set` 中，值可以是任何类型，类似于数组但没有重复的值
    - 无序：`Set` 中的值没有顺序，不会按照插入的顺序返回
    - 尺寸属性： `size` 属性可以获取 `Set` 中值的数量


**【总结】**
    - 键值对 vs 单一值：`Map` 存储键值对，`Set` 存储单一值
    - 唯一性： 在 `Map` 中，键是唯一的；在 `Set` 中，值是唯一的
    - 顺序：`Map` 会记住键值对的插入顺序，`Set` 不会


#### Proxy 与 Reflect

Proxy 与 Reflect 都是 ES6 为了操作对象引入的 API

**Proxy**

- Proxy 可以对目标对象的读取、函数调用等操作进行拦截。
- 不直接操作对象，而是像代理模式，通过对象的代码对象进行操作
- 通过构造函数新建实例时，是对目标对象进行浅拷贝，此时目标对象与代理对象指向同一内存地址（一起变）

**Reflect**

- `Reflect` 是一个用于操作对象的 API，提供了各种方法，这些方法与 `Proxy` 的方法对应，可以对对象进行操作
- `Reflect` 的方法可以对目标对象进行操作，但不会修改目标对象
- `Reflect` 对象使用函数的方式实现了 `Object` 的命令式操作

**为何Proxy要结合Reflect**

- 更清晰的代码结构：`Proxy` 用于拦截对象操作，`Reflect` 提供了对这些操作的底层方法
- 规范性和可读性：`Reflect` 方法与对象的操作一一对应，如 `Reflect.get()` 与 `obj[key]` 的读取 ...
- 避免直接操作对象：`Reflect` 提供间接访问目标对象的方式
- 不依赖代理对象的特定实现：因为 `Reflect` 是 ECMAScript 规范定义的标准方法
    ```js
    // 举个例子
    get (key) {
        return obj[key]; // 依赖浏览器特定的实现
        return Reflect.get(obj, key); // SCMAScript 规范定义的标准方法（具有不变性）
    }
    ```


#### 扩展

**字符串**

- 子串识别。`ES6` 之前只有 `indexOf`，`ES6` 之后 新增 `includes()` 、`startWidth()`[是否在原字符串头部] 、`endsWith()`[是否在原字符串尾部]
- 字符串重复：`repeat()`[返回新的字符串，表示将字符串重复指定次数返回]
- 字符串补全：`padStart()`[返回新的字符串，表示用参数字符串从头部（左侧）补全原字符串] 、`padEnd()`[返回新的字符串，表示用参数字符串从尾部（右侧）补全原字符串]
- 模版字符串：``

**数值**

- `Number` 对象新方法

    - `Number.parseInt()`[将给定字符串转化为指定进制的整数]
    - `Number.parsetFloat()`[将给定字符串转化为指定进制的浮点数`]
    - `Number.isInteger()`[是否为整数]
    - `Number.isSafeInteger()`[是否为安全整数]
    - `Number.isNaN()`[是否为 `NaN`]
    - `Number.isFinite()`[是否为有限数]

- `Math` 对象的扩展

    - `Math.cbrt()`[求一个数的立方根]
    - `Math.hypot()`[求一个数的平方和]
    - `Math.sign()`[求一个数的符号]
    - `Math.trunc()`[截断一个数的整数部分]


**对象**

- 对象字面量

    - 属性简写
    - 方法名简写
    - 属性名表达式

- 对象拓展运算符（对象解构）

- 对象的新方法

    - `Object.assign()`[将源对象的属性拷贝到目标对象]（浅拷贝）
    - `Object.is()`[判断两个值是否相等]


**数组**

- 数组创建

    - `Array.of`：[创建一个数组，将参数中所有值作为元素形成数组，返回一个数组]
    - `Array.from`：[创建一个数组，将类数组对象或可迭代对象转化为数组，返回一个数组]

- 扩展方法

    - 【查找】
        - `find`：[查找数组中符合条件的元素,若有多个符合条件的元素，则返回第一个符合条件的元素，没有则返回 `undefined`]
        - `findIndex`：[查找数组中符合条件的元素的索引，若没有则返回 -1]
    - 【填充】
        - `fill`：[将数组中指定位置的元素填充为指定值，"返回原数组"]
            - `fill(用来填充的值, 被填充的起始索引, 被填充的结束索引，默认为数组末尾)`
    - 【遍历】
        - `entries`：遍历键值对
        - `keys`：遍历键名
        - `values`：遍历键值
    - 【包含】
        - `includes`：[判断数组是否包含某个值，返回 `true` 或 `false`]
    - 【嵌套数组转一维数组】
        - `flat`：[将嵌套数组转为一维数组，返回新数组]




### 运算符与语句

#### 函数

**函数参数的扩展**

    - 默认参数
    - 不定参数

**箭头函数**

    - 语法：(参数) => {函数体}
        - 参数只有一个时，参数的括号()可以省略
        - 函数体中，只有一行语句时，return、花括号{}可以省略
        - 当箭头函数需要返回对象时，为了区分代码块，要用()将对象包裹起来
    -【注意点】没有 this、super、new.target、arguments
        - 箭头函数的 this 对象，是定义函数时的对象，而不是调用函数时的对象



#### 类 class

#### 模块

    - export 与 import

    ```js
    // 导出1
    export const test = () => {}
    // 导入1
    import { test } from 'your-module'
    // 使用 export const 导出的变量可以使用 {} 进行解构导入

    // 导出2
    const test1 = () => {};
    export default { test1 };
    // 导入2
    import myModule from 'your-module';
    const { test1 } = myModule;
    // 对于使用 export default 导出的对象，引入时需要使用 import myModule from 'your-module'; 的形式，而不是像命名导出那样使用 {} 来解构
    // 在一个模块中，export default 只能有一个

    // 导出3
    const test2 = () => {}
    export { test2 }
    // 导入3
    import { test2 } from 'your-module';
    // 使用 export 命名导出的变量可以使用 {} 进行解构导入
    ```

### 异步编程

#### Promise

**定义**
    
    - 是异步编程的一种解决方案
    - 从语法上来说，，Promise 是一个对象，可以获取异步操作的消息

**状态**

    - pending：表示异步操作正在执行
    - fulfilled：表示异步操作已经执行完成，且没有报错
    - rejected：表示异步操作已经执行完成，且报错
    
    - 状态改变：只能从 pending -> fulfilled 或 pending -> rejected。一旦处于 fulfilled 或 rejected 状态，就无法再从 pending -> fulfilled 或 pending -> rejected。

**then 方法**

    - then 方法的第一个参数是 resolve 方法，第二个参数是 reject 方法。
    - 两个方法只有一个会被调用
    - 简便的 Promise 链式编程最好保持扁平化，不要嵌套 Promise


#### Generator函数

**定义**

    - Generator 函数可以通过 yield 关键字，把函数的执行流程挂起，为改变执行流程提供了可能，从而为异步编程提供了解决方案

**Generator函数的组成**

    - function后面，函数名前面有个 “*”（用来表示函数为 Generator 函数）
    - 函数内部有 yield 表达式（用来定义函数内部的状态）

    ```js
    function* func(){
        console.log("one");
        yield '1';
        console.log("two");
        yield '2'; 
        console.log("three");
        return '3';
    }

    func.next();
    // one
    // {value: "1", done: false}
    
    func.next();
    // two
    // {value: "2", done: false}
    
    func.next();
    // three
    // {value: "3", done: true}
    
    func.next();
    // {value: undefined, done: true}
    ```


#### async函数

    - async 函数就是 Generator 函数的语法糖，将 Generator 函数的 yield 表达式，替换成 await 表达式
    - async 是 ES7 才有的与异步操作有关的关键字

**语法**
    - `async function name([param[, param[, ... param]]]) { statements }`

**返回值**
    - async 函数的返回值是 Promise 对象，可以使用 then() 方法添加回调函数
    - async 函数中可能会有 await 表达式。async 函数执行时，如果遇到 await 表达式，会等待其内部的 Promise 对象完成，然后继续执行后续的代码
