const express = require('express');
const paypal = require('@paypal/checkout-server-sdk');

const app = express();
const port = process.env.port || 3000;

// 配置 PayPal 环境
const clientId = 'CLIENT_ID';
const clientSecret = 'CLIENT_SECRET';
const environment = new paypal.core.SandboxEnvironment(clientId, clientSecret);
const client = new paypal.core.PayPalHttpClient()

// 路由 - 创建订单
app.post('/my-server/createOrder', async (req, res) => {
  const amount = req.body.amount;

  const request = new paypal.orders.OrdersCreateRequest();
  request.prefer('return=representation');
  request.requestBody({
    intent: 'CAPTURE',
    purchase_units: [{
      amount: {
        currency_code: 'USD',
        value: amount
      },
    }],
  });
  
  try {
    const response = await client.execute(request);
    res.json({ id: response.result.id });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// 路由 - 完成支付
app.post('/my-server/capture-paypal-order', async (req, res) => {
  const orderID = req.body.orderID;

  const request = new paypal.orders.OrdersCaptureRequest(orderID);
  request.requestBody({});

  try {
    const response = await client.execute(request);
    res.json(response.result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
})