## Paypal支付

### 引入方式

#### JavaScript SDK - https://developer.paypal.com/sdk/js/

**作用**
    - 渲染按钮
    - 渲染支付方式图标
    - 渲染信用卡和借记卡表单字段
    - 检查给定支付方式的资金资格

**SDK引入**
    `<script src="https://www.paypal.com/sdk/js?client-id=YOUR_CLIENT_ID"></script>`

**client_id获取**

    - 进入 https://developer.paypal.com/home/
    - 进行账号登录，点击右上角账号，选择 `Dashboard`，进入 https://developer.paypal.com/dashboard/
    - 选择 `Apps & Credentials`，点击 `Create App`