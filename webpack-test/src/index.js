// commonJS 引入的规范
// const say = require("./a")

// ESModule 引入的规范
import say from './a'
import { testFunc, s } from './es6'

import './css/index.css'
import './less/index.less'
import './sass/index.scss'

// JS引入IMG URL
const imgUrl = require('./img/crop-main.png')
console.log('imgUrl -- ', imgUrl);
const imgDom = document.querySelector('.img')
imgDom.src = imgUrl

function add(a, b) {
  console.log(a + b)
}

add(1, 2)
console.log(say)
console.log(' - this is index.js - ')

console.log(testFunc());
console.log(s.say());