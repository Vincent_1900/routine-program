import '@babel/polyfill'

function testFunc() {
  // 这是一个箭头函数
  setTimeout(() => {
    console.log('这是一个箭头函数');
  })
}

// 类
class Student {
  say () {
    console.log('这是一个学生类的方法');
    console.log([1,2,3,4,5].includes(1), ' -- includes -- ');
  }
}

const s = new Student()

export {
  testFunc,
  s
}