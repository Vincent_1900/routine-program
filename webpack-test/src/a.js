const say = function () {
  console.log('a.js - this is a say function');
}

console.log(' - this is a.js - ')

// commonJS 导出规范语法 -（浏览器不能直接识别）
// module.exports = {
//     say
// }

// ESMdule 导出规范语法 -（浏览器不能直接识别）
export default {
  say
}