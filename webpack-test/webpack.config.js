const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

// commonJS 语法规范
module.exports = {
  mode: 'development',
  // 单页面打包
  // entry: './src/index.js',
  // output: {
  //   filename: 'js/dist.js', // 打包后的文件名
  //   path: path.resolve(__dirname, './dist') // 打包后的路径，必须是绝对路径
  // },

  // 多页面打包（多个入口）
  entry: {
    home: './src/js/home.js',
    about: './src/js/about.js'
  },
  //（多个出口）
  output: {
    filename: 'js/[name].js', // 打包后的文件名
    path: path.resolve(__dirname, './dist') // 打包后的路径，必须是绝对路径
  },
  // watch: true, // 自动编译热部署
  // webpack-dev-server 的基本配置
  devServer: {
    open: true, // 开启默认浏览器
    static: './list', // 指定开启服务的目录，默认是 public
    port: 8081, // 指定服务的端口， 默认是 8080
    compress: true, // 资源压缩
    hot: true // 开启热更新
  },

  // 配置loader
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'] 
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader'] // 从右往左的顺序执行
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg)$/,
        use: {
          loader: 'url-loader',
          options: {
            esModule: false,
            limit: 159*1024, // 如果大于100kb的图片就不处理成base64
            name: '[hash:4].[ext]', // 修改打包图片的名字
            outputPath: 'imgs', // 配置图片输出路径
          }
        }
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            esModule: false,
          }
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/, // 除了这个文件之外
        options: {
          presets: ['@babel/env']
        } 
      },
      {
        test: require.resolve('jquery'),
        loader: 'expose-loader',
        options: {
          exposes: ['$', 'jQuery']
        }
      }
    ]
  },

  plugins: [
    // new HtmlWebpackPlugin({
    //   template: './src/index.html',
    //   filename: 'index.html' // 访问路径变成 localhost:8081/index.html
    // })
    new HtmlWebpackPlugin({
      template: './src/page/home.html',
      filename: 'home.html',
      chunks: ['home'], // 每个页面只加载自己的 js 文件
    }),
    new HtmlWebpackPlugin({
      template: './src/page/about.html',
      filename: 'about.html',
      chunks: ['about']
    }),
    new webpack.ProvidePlugin({
      $: 'jquery'
    })
  ],

  devtool: 'eval-source-map'
}