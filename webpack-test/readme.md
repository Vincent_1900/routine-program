**前期依赖环境：node - npm**

**指令**

- `npm init -y`  // 初始化工程，生成 package.json 文件

- `npm i webpack webpack-cli -D `// 开发模式下载依赖包





如果不配置 `webpack.config.json` 文件，则默认打包 `package.json` 文件 中 `main` 属性对应的值的文件，生成默认打包后生成路径为 `dist/main.js`





**`webpack` 几个核心模块**

- `mode` : 模式；`development` (开发；不会压缩代码)、`production` (生产；默认，会压缩代码)

- `entry` : 入口

- `output` : 出口

- `module` : 配置 `loader`; `loader` 是处理非JS的文件打包

- `plugins` : 查看，处理任务（删除、压缩等）





**`webpack` 配置文件**

- `webpack.config.js` (使用 `commonJS` 规范)

- `path.resolve() | __dirname` : 当前文件根目录

 【**注**】：通过 `webpack --config 配置文件名` 指定配置文件





**`webpack` 监听文件变化自动编译**

- `watch`

  - 在  `package.json`  文件中配置脚本  `webpack --watch`

  - 在  `webpack.config.js`  文件中配置  `watch:true`

    

- `webpack-dev-server`

  - 安装  `npm i webpack-dev-server -D`
  - `package.json`  文件配置  `'build': 'webpack-dev-server' `
    - 默认将  `public`  文件开启服务（此目录不会存在项目目录下，是保存在 **内存** 中的）
    - 将打包好的文件，放在内存中 【**注**：会发现在 `index.html` 中引入 `/dist.js` 也不会报错，是因为放在 **内存中** 的】

​    【**注**】默认访问的路径是 public 文件（然后将 `index.html` 迁移到 `public` 目录下）



- `webpack-dev-server` 其他命令
  - `--static` : 指定开启服务的目录 `webpack-dev-server --static` 文件名
  - `--open` : 开启默认浏览器 `webpack-dev-server --open`
  - `--port` : 设置服务端口 `webpack-dev-server --port 端口号``
  - `--compress` : 资源压缩 `webpack-dev-server --compress`
  - `--hot` : 热更新 `webpack-dev-server --hot`（自动编译改变的文件，不变的文件不编译）



- 关于 `webpack-dev-server` 的 `devServer.static` 与 `output` 的区别

  - 区别
    - `devServer.static`  是用来指定 **开发服务器** 服务的静态文件路径，主要用于 **开发环境**
    - `output.path` 是用来指定打包后的输出路径，主要用于 **生产环境**
    - 两哥配置是服务不同的环境

  - `webpack-dev-serve` 的作用

    - 在 **开发环境** 中，`webpack-dev-server` 会启动一个服务器来服务网页

    - 所有打包后的文件都在 **内存** 中，不会输出到实际的文件系统中（磁盘中）

    - 如果 `devServer.static` 中的静态文件路径包含了 `output.path`， 两者文件可能会相互覆盖，最好保证两个路径不会有交集





**`HTML`  插件  `html-webpack-plugin`**

- 安装： `npm i html-webpack-plugin --save-dev`

- 导入： `const HtmlWebpackPlugin = require('html-webpack-plugin')`

- 使用： `new HtmlWebpackPlugin({filename：文件名, template：模版})`
  - 根据 `template` 中指定的模版，自动生成 `filename` 的文件

- 做了什么事情
  - 通过模版生成一个 `html` 文件
  - 将 `html` 文件放在内存中
  - 自动将打包好的 `JS` 文件引入 `html` 中





**`loader` : 将非 `JS` 文件模块化**

- `CSS` 文件

- 安装：`npm i css-loader style-loader -D`

  【**注**】(`css-loader`：将 `css` 模块化 | `style-loader`：将 `css` 通过 `style` 标签的形式引入 `html` 文件中)

- 配置：`webpack.config.js` 文件

  	{
  	    test: /\.css$/,
  	    use: ['style-loader', 'css-loader']
  	}



- `less` 文件

  - 安装： `npm i less less-loader -D`

    【**注**】（`less`：解析 `less` 语法, `less-loader`：将 `less` 转为 `css`）

  - 配置：`webpack.config.js` 文件

     	{
     	    test: /\.less$/,
     	    use: ['style-loader', 'css-loader', 'less-loader']
     	}
     	



- `sass` 文件（文件后缀为 `.scss`）

  - 安装：`npm i node-sass sass-loader -D`

    【**注**】（`node-sass`：解析 `sass` 语法, `sass-loader`：将 `sass` 转为 `css`）

  - 配置：`webpack.config.js` 文件

   	{
   	    test: /\.less$/,
   	    use: ['style-loader', 'css-loader', 'sass-loader']
   	}
   	

  

- 图片文件

  - 展示图片的三种方式

    - `css` 中使用背景图 

      ```js
      /* css loader 默认处理了 */
      {
          test: /\.css$/,
          use: ['style-loader', {
              loader: 'css-loader',
              options: {
                  url: false, // 默认是true，即对css文件中的url引入的图片进行打包处理
              }
          }]
      }
      ```

    - `js` 导入

      ```js
      const imgUrl = require('图片路径')
      图片的dom对象.src = imgUrl
      ```

      - 安装：`npm i file-loader url-loader -D`

      - 配置：`webpack.config.js` 文件

        ```js
        {
        	test: /\.(png|jpg)$/,
            use: {
              	loader: 'url-loader',
                // 需要的配置项
                options: {
                    esModule: false,
                    limit: 100*1024, // 如果大于100kb的图片就不处理成base64
                    name: '[hash:4].[ext]', // 修改打包图片的名字
                }
            }
        }
        ```

    - 使用 `img` 标签

      - 下载：`npm i html-loader -D`

      - 配置：`webpack.config.js` 文件

        ```js
        {
        	test: /\.html$/,
        	use: {
                loader: 'html-loader',
                options: {
                    esModule: false
                }
            }
        }
        ```

        

- `babel`

  - 作用：将 高版本的 `js` 语法，转成 低版本浏览器能支持的语法

  - **`babel-loader @babel/core（核心） @babel/preset-env`**

    - 安装： `npm i babel-loader @babel/core（核心） @babel/preset-env -D`

      - 只转语法，不转API
      - 新API：（全局对象：`Promise, Proxy` 等；实例方法：`[].find(), ''.includes` 等）

    - 配置：在 `webpack.config.js` 文件中配置

      ```js
      rules: [
          ...
          {
             	test: /\.js$/,
              loader: 'babel-loader',
              exclude: /node_modules/, // 除了这个文件之外
              options: {
                  presets: ['@babel/env']
              } 
          } 
      ]
      ```

  - **`@babel/polyfill`**

    - 安装：`npm i @babel/polyfill -S`

    - 配置：

      - 在 `.js` 文件中引入

        ```js
        import '@babel/polyfill'
        ```

      - 在 `webpack.config.js` 文件中配置

        ```js
        module.exports = {
            entry: ['babel-polyfill', './src/js']
        }
        ```

  

  

**`SourceMap`**

- 作用：源码的映射。将打包好的代码，通过 `sourceMap` 找到源文件所在的位置

- 配置：

  ```js
  module.exports = {
      ...
      devtool: 'source-map' // 根据源文件生成对应的 .map 文件
  }
  ```

  - 不会生成 `.map` 的几种配置

    ```js
    - `false` ： 不使用 `source-map`
    - `none` ：生产环境默认值
    - `eval` ：开发环境默认值。会在 `eval` 执行的代码中添加 `//# sourceURL=`，代码调试也能定位到对应位置
    ```

  - 会生成 `.map` 的几种配置（`// #sourceMappingURL=dist.js.map`）

    ```js
    - `source-map`
    	- 定位到源代码
    - `cheap-source-map`
    	- 定位到转换后的代码（精确到行，忽略列）
    - `hidden-source-map`
    	- 定位到构建后代码的位置
    ```





 **【`loader` 总结】**

- 样式文件 的引入
  - 一定要在 `JS` 文件中引入 样式文件

    - 由于 样式文件 是非 `JS` 文件（不具备模块化），所以需要 引入 `loader` 将 样式文件 模块化

    - 需要在 `module` 模块中配置 `loader`





**多页面打包**

- 核心：多个入口、多个出口（有多个入口，就有多个出口）

- 配置：

  ```js
  module.export = {
  	...
      // 多页面打包（多个入口）
      entry: {
          home: './src/js/home.js',
          about: './src/js/about.js'
      },
      //（多个出口）
      output: {
          filename: 'js/[name].js', // 打包后的文件名
          path: path.resolve(__dirname, './dist') // 打包后的路径，必须是绝对路径
      },
      ...
      plugins: [
          new HtmlWebpackPlugin({
            template: './src/page/home.html',
            filename: 'home.html',
            chunks: ['home'], // 每个页面只加载自己的 js 文件
          }),
          new HtmlWebpackPlugin({
            template: './src/page/about.html',
            filename: 'about.html',
            chunks: ['about']
          })
      ]
  }
  ```





**`webpack` 将 第三方库 引入到全局**

- 场景：由于 第三方库 所有页面都要用到，需挂载在全局的 `window` 上

- 方案

  - `expose-loader` - 将第三方库挂载全局

    - 安装：`npm i expose-loader -D`

    - 配置：

      ```js
      module: {
          rules: [
              ...
              {
                  test: require.resolve('jquery'), // 挂载jquery，前提是要安装 jquery 包
                  loader: 'expose-loader',
                  options: {
                      exposes:['$', 'jQuery'], // 打包后可以通过 window.$ 访问 jquery
                  }
              }
          ]
      }
      
      // 使用的 JS文件 中需要进行第三方库资源的导入 - import $ from 'jquery'
      ```

  - webpack 自带的插件 - ProvidePlugin

    ```js
    const webpack = require('webpack')
    
    plugin: [
        ...
        new webpack.ProvidePlugin({
            $: 'jquery'
        })
    ]
    // 使用的 JS文件 中不需要进行第三方库资源的导入
    ```





**`webpack` 区分开发环境 和 生产环境**
